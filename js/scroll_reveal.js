window.sr = ScrollReveal();
sr.reveal('.animate1', { scale: 1, duration: 2000, distance:'100px', viewFactor: .5, easing: 'cubic-bezier(.2, 0.5, 0.5, 1)'});

sr.reveal('.animate2', { scale: 1, duration: 1000, distance:'0px', viewFactor: .5, easing: 'cubic-bezier(1, 0.5, 0.5, .5)'});

sr.reveal('.animate3', { scale: 1, duration: 1000, distance:'0px', viewFactor: .3, easing: 'cubic-bezier(1, 0.5, 0.5, .5)'});

sr.reveal('.animate4', { scale: 1, duration: 1000, origin: 'right', distance:'20px', viewFactor: .3, easing: 'cubic-bezier(1, 0.5, 0.5, .5)'});
